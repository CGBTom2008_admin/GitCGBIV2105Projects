package com.jt;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * 基于此对象解析请求
 */
@Component
public class DefaultRequestOriginParser
        implements RequestOriginParser {
    //http://localhost:8090/consumer/doRestEcho1?origin=app1
    //这个方法会在资源访问时,由底层进行调用,获取请求中数据,再与我们指定的授权规则进行比对
    @Override
    public String parseOrigin(HttpServletRequest request) {
        return request.getParameter("origin");//参数名自己定义
    }

}
