package com.jt.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @FeignClient 注解用于描述远程服务调用接口,这个接口不需要你写实现类,你
 * 只需要定义访问规则即可(例如请求方式,请求url,请求参数).
 * @FeignClient注解描述的接口的实现类对象会默认交给spring管理,这个bean对象
 * 的名字默认就是name属性指定的值,这个name还有一个层面的含义,就是你远程调用的
 * 服务名.
 * 说明:假如@FeignClient注解中添加了contextId属性,则这个属性值默认
 * 会作为当前bean对象的名字,此时name的值仅仅作为要调用的服务名对待,一般
 * 推荐contextId的值默认为@FeignClient注解描述的接口的名字(首字母小写)
 */
@FeignClient(name="sca-provider",
             contextId = "remoteProviderService",
             fallbackFactory = ProviderFallbackFactory.class)
public interface RemoteProviderService {
    //@GetMapping表示以get请求方式调用远端服务
    //"/provider/echo/{msg}"为远程调用服务的url
    @GetMapping("/provider/echo/{msg}")
    String echoMessage(@PathVariable("msg") String msg);
}
