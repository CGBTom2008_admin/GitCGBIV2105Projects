package com.jt.feign;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;
//此对象可以作为Feign方式的远程调用异常处理对象
@Component
public class ProviderFallbackFactory
        implements FallbackFactory<RemoteProviderService> {
    //当正常远程调用的服务不可用时,系统可以调用此方法进行请求处理
    @Override
    public RemoteProviderService create(Throwable throwable) {
        return new RemoteProviderService() {//匿名内部类
            @Override
            public String echoMessage(String msg) {
                //...通知运维人员(发短信,发邮件,电话)...
                return "服务维护中";
            }
        };
    }
}
