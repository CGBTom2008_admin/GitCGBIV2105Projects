package com.jt.cache;

public class SynchronizedCache implements Cache {
    private Cache cache;

    public SynchronizedCache(Cache cache) {
        this.cache = cache;
    }

    @Override
    public synchronized void putObject(Object key, Object value) {
        cache.putObject(key, value);
    }

    @Override
    public synchronized Object getObject(Object key) {
        return cache.getObject(key);
    }

    @Override
    public synchronized Object removeObject(Object key) {
        return cache.removeObject(key);
    }

    @Override
    public void clear() {
        cache.clear();
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public String toString() {
        return "SynchronizedCache{" +
                "cache=" + cache +
                '}';
    }

    public static void main(String[] args) {
        Cache cache = new SynchronizedCache(new DefaultCache());
        cache.putObject("A", 100);
        cache.putObject("B", 200);
        System.out.println(cache);
    }
}