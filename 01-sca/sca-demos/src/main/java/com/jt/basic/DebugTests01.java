package com.jt.basic;

public class DebugTests01 {
    static int doGet(){
        int count=100;
        try{
            count++;
            count=count/0;
            return count;
        }catch (Exception e){
            count=100;
            return count;
        }finally{
            count=200;
        }
    }
    public static void main(String[] args) {
        int count=doGet();
        System.out.println(count);
    }
}
