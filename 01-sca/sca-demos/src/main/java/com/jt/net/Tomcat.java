package com.jt.net;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Tomcat {
    public static void main(String[] args) throws IOException {
        //Java中服务的创建
        System.out.println("start server ...");
        //Java中网络编程中的服务对象类型为ServerSocket
        //构建服务,并在9999端口进行监听
        ServerSocket server=new ServerSocket(9999);

        //等待服务端的连接
        while(true){
            //这里的socket代表客户端对象
            Socket socket=server.accept();//阻塞式方法
            System.out.println("hello client "+socket);
            //获取向客户端输出数据的流对象
            OutputStream out=socket.getOutputStream();
            //向客户端写数据
            //out.write("hello".getBytes());//这种方式浏览器不会显示
            String content="HTTP/1.1 200\r\n" +//响应行
                    "Content-Type: text/html;charset=UTF-8\r\n"+//响应头
                    "\r\n"+//空行,
                    "<h1>hello client</h1>"; //数据
            out.write(content.getBytes());
            socket.close();
        }
    }
}
