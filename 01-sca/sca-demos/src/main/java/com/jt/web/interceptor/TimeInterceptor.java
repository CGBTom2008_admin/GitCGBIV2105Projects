package com.jt.web.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

public class TimeInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("==preHandler==");
        LocalTime now=LocalTime.now();//JDK8中的时间对象
        int hour=now.getHour();//获取当前时间对应小时
        System.out.println("hour="+hour);
        if(hour<=6||hour>=23)
            throw new RuntimeException("请在6~17点进行访问");
        return true;//false表示请求到此结束,true表示执行执行链中的下一个对象
    }

}
