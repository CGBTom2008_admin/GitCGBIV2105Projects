package com.jt.web.controller;

import com.jt.aop.RequiredLog;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo/")
public class DemoController {//--->Controller--->Service--->Dao
    @RequiredLog //由此注解描述的方法就是切入点方法
    @GetMapping
    public String doSayHello(){
        //System.out.println("Before:"+System.currentTimeMillis());
        System.out.println("==doSayHello==");
        String result="hello spring";
        //System.out.println("After:"+System.currentTimeMillis());
        return result;
    }
}
