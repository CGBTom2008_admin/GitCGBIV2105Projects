package com.jt.web.config;

import com.jt.web.interceptor.TimeInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SpringWebConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册拦截器,并且设置要拦截的路径
        registry.addInterceptor(new TimeInterceptor())
                .addPathPatterns("/demo/");//要拦截的路径
    }
}
