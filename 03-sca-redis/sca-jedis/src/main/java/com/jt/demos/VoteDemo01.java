package com.jt.demos;

import redis.clients.jedis.Jedis;

import java.util.Set;

/**
 * 设计一个基于某个活动的投票系统.
 */
public class VoteDemo01 {

    //注意,将来这个连接不能多线程共享
    private static  Jedis jedis=new Jedis("192.168.126.130",6379);

    /**
     * 判定userId是否参与过activityId这个活动的投票
     * @param activityId
     * @param userId
     * @return true表示参与过投票
     */

    static boolean checkVote(String activityId,String userId){
        //判断set集合activityId对应的值中是否包含这个userId
       return jedis.sismember(activityId,userId);
    }

    /**
     * 执行投票逻辑
     * @param activityId
     * @param userId
     * @return true 表示投票ok
     */
    static boolean addVote(String activityId,String userId){
        if(checkVote(activityId,userId)){
            System.out.println("您已经参与过这个投票");
            return false;
        }
        jedis.sadd(activityId,userId);
        return true;
    }

    /**
     * 获取活动的总票数
     * @param activityId
     * @return
     */
    static Long getActivityVotes(String activityId){
        return jedis.scard(activityId);
    }
    /**
     * 获取参与了投票的用户id
     * @param activityId
     * @return
     */
    static Set<String>  getActivityUsers(String activityId){
       return jedis.smembers(activityId);
    }

    public static void main(String[] args) {
        //1.设置一个活动
        //1)获取id
        String activityId="10001";
        //2)活动标题
        //3)活动内容
        //2.基于活动进行投票设计
        String user1="201";
        String user2="202";
        String user3="203";
        //2.1 一个用户对同一个活动不能重复投票
        //boolean flag=checkVote(activityId,user1);
        //System.out.println("flag="+flag);
        //2.2 执行投票
        addVote(activityId,user1);
        addVote(activityId,user2);
        addVote(activityId,user3);
        //2.3 能够显示参与投票的人数
        long voteCount=getActivityVotes(activityId);
        System.out.println(voteCount);
        //2.4 管理员可以查看哪些人参与了投票
        Set<String> userIds=getActivityUsers(activityId);
        System.out.println(userIds);
    }
}
