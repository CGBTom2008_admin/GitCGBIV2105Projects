package com.jt.redis.demos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * 定义一个简易的秒杀队列,算法FIFO
 */
//@Component
@Repository //@Repository 注解用于描述数据层对象
public class SecondKillQueue {
//    @Autowired
//    private StringRedisTemplate stringRedisTemplate;

    //这种写法:参考官网https://spring.io/projects/spring-data-redis#overview
    @Resource(name = "stringRedisTemplate")
    private ListOperations<String,String> lo;
    /**执行入队操作*/
    public void enque(String msg){
//        ListOperations<String, String> lo =
//                stringRedisTemplate.opsForList();
        lo.leftPush("killQueue", msg);
    }
    /**执行出队操作*/
    public String deque(){
//        ListOperations<String, String> lo =
//                stringRedisTemplate.opsForList();
        return lo.rightPop("killQueue");
    }
}
