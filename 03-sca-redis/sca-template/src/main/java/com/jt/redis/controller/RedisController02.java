package com.jt.redis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
//springboot+redis+aop

@RestController
@RequestMapping("/redis/demo02")
public class RedisController02 {
    //@Autowired
    //private RedisTemplate redisTemplate;
    @Cacheable(value="blogCache")//存数据到cache
    @GetMapping
    public Map<String,Object> list(){
        //redisTemplate.opsForHash().get()
        System.out.println("==select data from database===");
        //假如下map中的数据来自mysql
        Map<String,Object> map=new HashMap<>();
        map.put("id", 100);
        map.put("name", "jack");
        //redisTemplate.opsForHash().put();
        return map;
    }

    //清除cache中数据
    @GetMapping("/clear")
    @CacheEvict(value = "blogCache",beforeInvocation = false,allEntries = true)
    public String update(){
        return "clear after update";
    }
}
