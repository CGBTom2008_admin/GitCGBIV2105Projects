package com.jt.redis.demos;

import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class IdGenerator {
    @Resource(name = "stringRedisTemplate")
    private ValueOperations valueOperations;
    /**基于redis生成一个递增的id值*/
    public Long generatorId(){
        return valueOperations.increment("id");
    }
}
