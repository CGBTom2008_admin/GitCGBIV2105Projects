package com.jt;

import com.jt.redis.demos.CartService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
public class CartServiceTests {

    @Autowired
    private CartService cartService;

    @Test
    void testAddCart(){
        cartService.addCart(101, "201", 1);
        cartService.addCart(101, "201", 2);
    }
    @Test
    void testListCart(){
        Map<String, Object> stringObjectMap =
                cartService.listCart(101);
        System.out.println(stringObjectMap);
    }
}
