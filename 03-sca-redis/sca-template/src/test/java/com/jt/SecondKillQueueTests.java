package com.jt;

import com.jt.redis.demos.SecondKillQueue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SecondKillQueueTests {
    @Autowired
    private SecondKillQueue queue;
    @Test
    void testEnque(){
       queue.enque("A");
       queue.enque("B");
       queue.enque("C");
    }
    @Test
    void testDeque(){
        System.out.println(queue.deque());
        System.out.println(queue.deque());
        System.out.println(queue.deque());
    }
}
