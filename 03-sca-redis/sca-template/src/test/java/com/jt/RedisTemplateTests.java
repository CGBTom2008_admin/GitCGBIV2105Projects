package com.jt;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@SpringBootTest
public class RedisTemplateTests {
    /**RedisTemplate对象也Spring data模块提供的一个操作redis数据库数据的API对象,
     * 此对象主要用于操作redis数据库中的复杂数据类型数据,默认序列化方式为
     * JdkSerializationRedisSerializer*/
    @Autowired
    private RedisTemplate redisTemplate;//将来可以按照自己的业务需求定制RedisTempalte

    @Test
    void testSetData(){
        SetOperations setOperations=redisTemplate.opsForSet();
        setOperations.add("setKey1", "A","B","C","C");
        Object members=setOperations.members("setKey1");
        System.out.println("setKeys="+members);
        //........
    }
    @Test
    void testListData(){
        //向list集合放数据
        ListOperations listOperations = redisTemplate.opsForList();
        listOperations.leftPush("lstKey1", "100"); //lpush
        listOperations.leftPushAll("lstKey1", "200","300");
        listOperations.leftPush("lstKey1", "100", "105");
        listOperations.rightPush("lstKey1", "700");
        Object value= listOperations.range("lstKey1", 0, -1);
        System.out.println(value);
        //从list集合取数据
        Object v1=listOperations.leftPop("lstKey1");//lpop
        System.out.println("left.pop.0="+v1);
        value= listOperations.range("lstKey1", 0, -1);
        System.out.println(value);
    }

    @Test
    void testHashOper01(){
        HashOperations ho = redisTemplate.opsForHash();
        ho.put("blog", "id", 101);
        ho.put("blog", "title", "redis template");
        List blog = ho.values("blog");

        System.out.println(blog);
        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id", 201);
        map.put("name", "redis");
        ho.putAll("tag",map);
        Object name= ho.get("tag", "name");
        System.out.println(name);
    }

    @Test
    void testStringOper01(){
        //默认采用了JDK的序列化方式,存储key/value默认会将key/value都转换为字节再进行存储
        //redisTemplate.setKeySerializer(new JdkSerializationRedisSerializer());
        //自己设置序列化方式
        //redisTemplate.setKeySerializer(new StringRedisSerializer());
        //redisTemplate.setValueSerializer(new StringRedisSerializer());
        ValueOperations vo =
                redisTemplate.opsForValue();
        vo.set("token", UUID.randomUUID().toString());
        Object token = vo.get("token");
        System.out.println(token);
    }
}
