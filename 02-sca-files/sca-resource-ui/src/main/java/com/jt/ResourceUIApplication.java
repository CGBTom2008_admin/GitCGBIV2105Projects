package com.jt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResourceUIApplication {
    public static void main(String[] args) {
        SpringApplication.run(ResourceUIApplication.class,args);
    }
}
