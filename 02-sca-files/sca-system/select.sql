#基于用户id查询角色id
select role_id from tb_user_roles where user_id=1; #1
#基于角色id查询菜单id
select menu_id from tb_role_menus where role_id=1; #1,2,3
#基于菜单id获取访问菜单需要的权限 (sys:res:create,....)
select permission from tb_menus where id in (1,2,3);

# 嵌套查询
select permission
from tb_menus
where id in (select menu_id
             from tb_role_menus
             where role_id=(
                 select role_id
                 from tb_user_roles
                 where user_id=1
             ));
#多表关联查询方案一
select distinct m.permission
from tb_user_roles ur join tb_role_menus rm on ur.role_id=rm.role_id
     join tb_menus m on rm.menu_id=m.id
where ur.user_id=1;

#多表关联查询方案二
select distinct m.permission
from tb_user_roles ur,tb_role_menus rm,tb_menus m
where ur.role_id=rm.role_id and rm.menu_id=m.id and ur.user_id=1

