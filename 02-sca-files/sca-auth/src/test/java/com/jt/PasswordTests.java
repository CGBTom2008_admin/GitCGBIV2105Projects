package com.jt;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
public class PasswordTests {
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    //测试密码加密
    @Test
    void testEncode(){
        //1.定义密码
        String rawPassword="123456";
        //2.对密码进行加密
        String encodedPassword=passwordEncoder.encode(rawPassword);
        System.out.println(encodedPassword);
        //$2a$10$oanLze/2MvTUTZxEB9qIwODJLBpMxUt2wctGqDVVrUWKmJmvX2IKO
        //$2a$10$PSrTCh70PWv9ewARgR/5ieY/JBGRPeD0O90CuNY9p5Hw2.1sMdUba
        //3.检测密码
        boolean flag=
        passwordEncoder.matches("123456",encodedPassword);
        System.out.println("flag="+flag);

    }

}
