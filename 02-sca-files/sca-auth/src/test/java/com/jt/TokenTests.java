package com.jt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
public class TokenTests {

    @Test
    void testUUID(){
        String token=UUID.randomUUID().toString();
        System.out.println(token);
        //ae47f132-f21e-4a18-a4d8-14781fd1d1c7
    }
}
