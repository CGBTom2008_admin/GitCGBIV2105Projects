package com.jt.auth.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Arrays;

/**
 * 完成所有配置的组装,在这个配置类中完成认证授权,JWT令牌签发等配置操作
 * 1)SpringSecurity (安全认证和授权)
 * 2)TokenConfig
 * 3)Oauth2(暂时说)
 */

@AllArgsConstructor
@Configuration
@EnableAuthorizationServer //开启认证和授权服务
public class Oauth2Config extends AuthorizationServerConfigurerAdapter {
    //此对象负责完成认证管理
    private AuthenticationManager authenticationManager;
    //TokenStore负责完成令牌创建,信息读取
    private TokenStore tokenStore;
    //负责获取用户详情信息(username,password,client_id,grant_type,client_secret)
    //private ClientDetailsService clientDetailsService;
    //JWT令牌转换器(基于用户信息构建令牌,解析令牌)
    private JwtAccessTokenConverter jwtAccessTokenConverter;
    //密码加密匹配器对象
    private PasswordEncoder passwordEncoder;
    //负责获取用户信息信息
    private UserDetailsService userDetailsService;

    //设置认证端点的配置(/oauth/token),客户端通过这个路径获取JWT令牌
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
        //配置认证管理器
        .authenticationManager(authenticationManager)
        //验证用户的方法获得用户详情
        .userDetailsService(userDetailsService)
        //要求提交认证使用post请求方式,提高安全性
        .allowedTokenEndpointRequestMethods(HttpMethod.POST,HttpMethod.GET)
        //要配置令牌的生成,由于令牌生成比较复杂,下面有方法实现
        .tokenServices(tokenService());//这个不配置,默认令牌为UUID.randomUUID().toString()
    }

    //定义令牌生成策略
    @Bean
    public AuthorizationServerTokenServices tokenService(){
        //这个方法的目标就是获得一个令牌生成器
        DefaultTokenServices services=new DefaultTokenServices();
        //支持令牌刷新策略(令牌有过期时间)
        services.setSupportRefreshToken(true);
        //设置令牌生成策略(tokenStore在TokenConfig配置了,本次我们应用JWT-定义了一种令牌格式)
        services.setTokenStore(tokenStore);
        //设置令牌增强(固定用法-令牌Payload部分允许添加扩展数据,例如用户权限信息)
        //JWT令牌必须设置，假如普通令牌就不要设置了
        TokenEnhancerChain chain=new TokenEnhancerChain();
        chain.setTokenEnhancers(
                Arrays.asList(jwtAccessTokenConverter));
        //令牌增强对象设置到令牌生成
        services.setTokenEnhancer(chain);
        //设置令牌有效期
        services.setAccessTokenValiditySeconds(3600);//1小时
        //刷新令牌应用场景：一般在用户登录系统后，令牌快过期时，系统自动帮助用户刷新令牌，提高用户的体验感
        services.setRefreshTokenValiditySeconds(3600*72);//3天
        //配置客户端详情
        //services.setClientDetailsService(clientDetailsService);
        return services;
    }

    //设置客户端详情类似于用户详情，客户端传递到服务端的信息，是哪些信息时，服务端才会颁发令牌
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
        //客户端id
        .withClient("gateway-client")
        //客户端秘钥
        .secret(passwordEncoder.encode("123456"))
        //设置权限
        .scopes("all")//all只是个名字而已和写abc效果相同
        //允许客户端进行的操作  里面的字符串千万不能写错
        .authorizedGrantTypes("password","refresh_token");
    }
    // 认证成功后的安全约束配置,由oauth2协议提供
    // 这里规定了我们通过什么url访问令牌，检查令牌
    // 而且这些url不需要认证就可以访问
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        //认证通过后,允许客户端进行哪些操作
        security
        //公开oauth/token_key端点
        .tokenKeyAccess("permitAll()")
        //公开oauth/check_token端点
        .checkTokenAccess("permitAll()")
        //允许提交请求进行认证(申请令牌)
        .allowFormAuthenticationForClients();
    }
}

