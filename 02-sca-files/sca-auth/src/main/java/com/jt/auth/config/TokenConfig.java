package com.jt.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * 创建JWT令牌配置类,基于这个类实现令牌对象的创建和解析.
 * JWT令牌的构成有三部分构成:
 * 1)HEADER (头部信息:令牌类型,签名算法)
 * 2)PAYLOAD (数据信息-用户信息,权限信息,令牌失效时间,...)
 * 3)SIGNATURE (签名信息-对header和payload部分进行加密签名)
 */
@Configuration
public class TokenConfig {
    //定义令牌签发口令(暗号),这个口令自己定义即可
    //在对header和PAYLOAD部分进行签名时,需要的一个口令
    private String SIGNING_KEY= "auth";
    //初始化令牌生成策略(默认生成策略 UUID)
    //这里我们采用JWT方式生成令牌
    @Bean
    public TokenStore tokenStore(){
        return new JwtTokenStore(jwtAccessTokenConverter());
    }
    //构建JWT令牌转换器对象,基于此对象创建令牌,解析令牌
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter converter=new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }
}
